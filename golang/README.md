# GO TIPS


## Stack

1. Event Based Messaging

## System

1. Ujian
1. Siswa

### API
1. pls see openapi.yaml

### Entities

Soal
| id | pertanyaan | solusi |
| ---| --- | ---|
|int | string | string |

Lembar Jawab
| id | user_id | soal_id | jawaban |
| --- | --- | --- | --- |
| int | int (siswa) | int (soal)| string |


Siswa
| id | nama | lulus |
| --- | --- | --- |
| int | string | boolean |

### Flow

1. Peserta menjawab hit POST /jawab pada ujian-api
2. ujian-api store data Lembar Jawab
3. ujian-api emit data Lembar Jawab ke kafka
4. siswa-api consume data Lembar jawab dari kafka
5. siswa-api update data Siswa lulus berdasarkan Lembar Jawab tersubmit

### Diagram

```mermaid
graph
    ui --> ujian_api
    public -- push --> kafka_in
    ujian_api -- pull --> kafka_in
    ujian_api -- push --> kafka_event
    siswa_api -- pull --> kafka_event
    tsl_calc -- pull --> kafka_event
    tsl_calc -- push --> kafka_out
    tsl_intgration -- pull --> kafka_out
    tsl_intgration --> TSL
```

