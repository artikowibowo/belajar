package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/Shopify/sarama"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"

	"gitlab.com/hsi/ujian"
)

const (
	Port     = "8001"
	Consumer = "localhost:9092"
	Topic    = "hsi.ujian"
	Group    = "group.siswa"
)

//data initialization
var (
	dataSiswa = map[int]Siswa{
		1: {ID: 1, Nama: "Alif"},
		2: {ID: 2, Nama: "Ba"},
		3: {ID: 3, Nama: "Ta"},
	}
)

func main() {
	//consumer init
	ctx := context.Background()
	topics := []string{Topic}

	saramaConfig := sarama.NewConfig()
	saramaConfig.Version = sarama.V2_1_1_0
	saramaConfig.Consumer.Return.Errors = true

	consumerGroup, err := sarama.NewConsumerGroup([]string{Consumer}, Group, saramaConfig)
	if err != nil {
		panic(err)
	}

	defer func() {
		if err := consumerGroup.Close(); err != nil {
			log.Fatalln(err)
		}
	}()

	go func() {
		for err := range consumerGroup.Errors() {
			fmt.Println("[siswa-api] ERROR", err)
		}
	}()

	//service init
	svc := &Service{
		DataSiswa: dataSiswa,
	}

	//http init
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	r.Get("/siswa", func(w http.ResponseWriter, r *http.Request) {
		out, err := svc.ListSiswa()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("service error: " + err.Error()))
			return
		}

		w.Header().Set("Content-Type", "application/json")

		payload, err := json.Marshal(out)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("service error: " + err.Error()))
			return
		}

		w.Write(payload)
	})

	log.Println("[siswa-api] booting up ", Port)
	go http.ListenAndServe(":"+Port, r)

	// consumer start
	log.Println("[siswa-api] connecting kafka consumer: " + Consumer)
	handler := consumerGroupHandler{svc: svc}
	for {

		err := consumerGroup.Consume(ctx, topics, handler)
		if err != nil {
			panic(err)
		}
	}
}

type consumerGroupHandler struct {
	svc *Service
}

func (consumerGroupHandler) Setup(_ sarama.ConsumerGroupSession) error   { return nil }
func (consumerGroupHandler) Cleanup(_ sarama.ConsumerGroupSession) error { return nil }
func (h consumerGroupHandler) ConsumeClaim(sess sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for msg := range claim.Messages() {
		log.Printf("[siswa-api] consumed message topic:%q partition:%d offset:%d\n", msg.Topic, msg.Partition, msg.Offset)
		var in ujian.LembarJawab
		if err := json.Unmarshal(msg.Value, &in); err != nil {
			log.Println("[siswa-api] fail consume message: ", err)
			continue
		}

		h.svc.Process(in)
		sess.MarkMessage(msg, "")
	}
	return nil
}
