package main

import (
	"log"

	"gitlab.com/hsi/ujian"
)

type Service struct {
	DataSiswa map[int]Siswa
}

func (s *Service) ListSiswa() (listSiswa []Siswa, err error) {
	for _, ls := range s.DataSiswa {
		listSiswa = append(listSiswa, ls)
	}
	return
}

func (s *Service) Process(in ujian.LembarJawab) {
	if data, exist := s.DataSiswa[in.UserID]; !exist {
		log.Println("[siswa-api] tidak ditemukan: ", in.UserID)
		return
	} else {
		data.Lulus = true
		s.DataSiswa[in.UserID] = data
		log.Println("[siswa-api] diluluskan: ", data.Nama)
	}
}
