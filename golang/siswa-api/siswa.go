package main

type Siswa struct {
	ID    int    `json:"id"`
	Nama  string `json:"nama"`
	Lulus bool   `json:"lulus"`
}
