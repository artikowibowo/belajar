package ujian

type LembarJawab struct {
	ID      int    `json:"id"`
	UserID  int    `json:"user_id"`
	SoalID  int    `json:"soal_id"`
	Jawaban string `json:"jawaban"`
}
