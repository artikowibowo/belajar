package main

import (
	"encoding/json"
	"log"

	"github.com/Shopify/sarama"
	"gitlab.com/hsi/ujian"
)

type Service struct {
	DataSoal        map[int]Soal
	DataLembarJawab map[int]ujian.LembarJawab

	Broker sarama.SyncProducer

	counter int
}

func (s *Service) JawabUjian(in ujian.LembarJawab) (err error) {
	s.counter += 1
	in.ID = s.counter
	s.DataLembarJawab[s.counter] = in
	s.emit(in)
	return
}

func (s *Service) ListLembar() (listLembar []ujian.LembarJawab, err error) {
	for _, lj := range s.DataLembarJawab {
		listLembar = append(listLembar, lj)
	}
	return
}

func (s *Service) emit(in ujian.LembarJawab) {
	payload, _ := json.Marshal(in)
	msg := &sarama.ProducerMessage{Topic: Topic, Value: sarama.StringEncoder(payload)}
	partition, offset, err := s.Broker.SendMessage(msg)
	if err != nil {
		log.Printf("[ujian-api] FAILED to emit message: %s\n", err)
	} else {
		log.Printf("[ujian-api] message emitted to partition %d at offset %d\n", partition, offset)
	}

}
