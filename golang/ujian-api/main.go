package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"

	"github.com/Shopify/sarama"

	"gitlab.com/hsi/ujian"
)

const (
	Port   = "8000"
	Broker = "localhost:9092"
	Topic  = "hsi.ujian"
)

//data initialization
var (
	dataSoal = map[int]Soal{
		1: Soal{ID: 1, Pertanyaan: "Soal no 1?", Solusi: "A"},
		2: Soal{ID: 2, Pertanyaan: "Soal no 2?", Solusi: "B"},
		3: Soal{ID: 3, Pertanyaan: "Soal no 3?", Solusi: "C"},
	}

	dataLembarJawab = map[int]ujian.LembarJawab{}
)

func main() {
	//broker init
	log.Println("[ujian-api] connecting kafka producer: " + Broker)
	producer, err := sarama.NewSyncProducer([]string{Broker}, nil)
	if err != nil {
		log.Fatalln(err)
	}
	defer func() {
		if err := producer.Close(); err != nil {
			log.Fatalln(err)
		}
	}()

	//service init
	svc := &Service{
		DataSoal:        dataSoal,
		DataLembarJawab: dataLembarJawab,
		Broker:          producer,
	}

	//http init
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Post("/jawab", func(w http.ResponseWriter, r *http.Request) {
		var (
			err error
			in  ujian.LembarJawab
		)

		if err = json.NewDecoder(r.Body).Decode(&in); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("service error: " + err.Error()))
			return
		}

		if err = svc.JawabUjian(in); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("service error: " + err.Error()))
			return
		}

		w.Write([]byte("recorded"))
	})

	r.Get("/jawab", func(w http.ResponseWriter, r *http.Request) {
		out, err := svc.ListLembar()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("service error: " + err.Error()))
			return
		}

		w.Header().Set("Content-Type", "application/json")

		payload, err := json.Marshal(out)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("service error: " + err.Error()))
			return
		}

		w.Write(payload)
	})

	log.Println("[ujian-api] booting up ", Port)
	http.ListenAndServe(":"+Port, r)
}
