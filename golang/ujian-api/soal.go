package main

type Soal struct {
	ID         int    `json:"id"`
	Pertanyaan string `json:"question"`
	Solusi     string `json:"-"`
}
